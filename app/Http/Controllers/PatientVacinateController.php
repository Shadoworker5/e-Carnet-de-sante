<?php

namespace App\Http\Controllers;

use App\Models\Patient_vaccinate;
use App\Models\Patients;
use App\Models\Vaccine_calendar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PatientVacinateController extends Controller
{
    protected $image_path;

    public function __construct(){
        // $this->middleware(['authadmin', 'authsupervisor']);
        // $this->middleware('log')->only('index');
        // $this->middleware('subscribed')->except('store');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    protected function userGuard(){
        if(in_array(Auth::user()->user_role, ['guest'])){
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->userGuard();
        $vaccines = Vaccine_calendar::all();
        return view('vaccines.vacinate_patient', ['vaccines' => $vaccines]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->userGuard();

        $this->validate($request, [
            'patient_code'          => 'required|min:8',
            'vaccine_name'          => 'required',
            'date_vaccinate'        => 'required|date',
            'time_vaccinate'        => 'required',
            'doctor_name'           => 'required|min:5',
            'doctor_phone'          => 'required|min:10',
            'lot_number_vaccine'    => 'required|min:5',
        ]);

        if ($request->file('image_path') !== null && $request->file('image_path')->isValid()){
            $this->image_path = $request->file('image_path')->getClientOriginalName();

            $request->image_path->move(public_path('flacon_images'), $this->image_path);
        }

        $patient_id = Patients::where('code_patient', '=', $request->patient_code)->get()->toArray()[0]['id'];

        Patient_vaccinate::create([
            'user_id'               => Auth::id(),
            'patient_id'            => $patient_id,
            'vaccine_calendar_id'   => $request->vaccine_name,
            'date_vacination'       => $request->date_vaccinate,
            'time_vacination'       => $request->time_vaccinate,
            'name_doctor'           => $request->doctor_name,
            'doctor_contact'        => $request->doctor_phone,
            'lot_number_vacine'     => $request->lot_number_vaccine,
            'vacine_status'         => '1',
            'rappelle'              => $request->rappelle !== "" ? $request->rappelle : null,
            'path_capture'          => $request->image_path !== "" ? $this->image_path : null
        ]);

        if(Auth::user()->user_role === 'collector'){
            return redirect(route('offline_submission'));
        }
        return redirect(route('patient.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Patient_vaccinate  $patient_vaccinates
     * @return \Illuminate\Http\Response
     */
    public function show($patient_vaccinates)
    {
        $this->userGuard();

        $vaccine_info = Patient_vaccinate::findOrFail($patient_vaccinates);
        $code_patient = Patients::findOrFail($vaccine_info->patient_id);

        return view('vaccines.detail', ['vaccine_info' => $vaccine_info, 'patient_code' => $code_patient->code_patient]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\patient_vaccinates  $patient_vaccinates
     * @return \Illuminate\Http\Response
     */
    public function edit($patient_vaccinates)
    {
        $this->userGuard();

        $vaccines = Vaccine_calendar::all();
        $vaccine_info = Patient_vaccinate::findOrFail($patient_vaccinates);
        $patient_code = Patients::findOrFail($vaccine_info->patient_id);

        return view('vaccines.edit', ['vaccine_info' => $vaccine_info, 'vaccines' => $vaccines, 'patient_code' => $patient_code->code_patient]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\patient_vaccinates  $patient_vaccinates
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->userGuard();

        $this->validate($request, [
            'patient_code'          => 'required|min:8',
            'vaccine_name'          => 'required',
            'date_vaccinate'        => 'required|date',
            'time_vaccinate'        => 'required',
            'doctor_name'           => 'required|min:5',
            'doctor_phone'          => 'required|min:12|regex:/^\+/',
            'lot_number_vaccine'    => 'required|min:5',
        ]);

        if ($request->file('image_path') !== null && $request->file('image_path')->isValid()){
            $this->image_path = $request->file('image_path')->getClientOriginalName();

            $request->image_path->move(public_path('flacon_images'), $this->image_path);
        }

        $patient_id = Patients::where('code_patient', '=', $request->patient_code)->get()->toArray()[0]['id'];

        $vaccines = Patient_vaccinate::findOrFail($id);
        $vaccines->update([
            'user_id'               => Auth::id(),
            'patient_id'            => $patient_id,
            'vaccine_calendar_id'   => $request->vaccine_name,
            'date_vacination'       => $request->date_vaccinate,
            'time_vacination'       => $request->time_vaccinate,
            'name_doctor'           => $request->doctor_name,
            'doctor_contact'        => $request->doctor_phone,
            'lot_number_vacine'     => $request->lot_number_vaccine,
            'vacine_status'         => '1',
            'rappelle'              => $request->rappelle !== "" ? $request->rappelle : null,
            'path_capture'          => $request->image_path !== "" ? $this->image_path : null
        ]);

        if(Auth::user()->user_role === 'collector'){
            return redirect(route('offline_submission'));
        }
        return redirect('patient');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\patient_vaccinates  $patient_vaccinates
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patient_vaccinate $patient_vaccinates)
    {
        //
    }

    public function addVacinate($patient_code){
        $this->userGuard();

        $vaccines = Vaccine_calendar::all();

        return view('vaccines.vacinate_patient', ['vaccines' =>$vaccines, 'patient_code' => $patient_code]);
    }
}
