<!-- Web Application Manifest -->
<link rel="manifest" href="/manifest.json">

<link rel="shortcut icon" href="/images/icon-72x72.png" type="image/x-icon">
<link rel="manifest" href="/manifest.json">
<meta name="theme-color" content="#00FA9A">

<meta name="description" content="Application de vaccination en ligne">
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="application-name" content="e-Santé">
<meta name="apple-mobile-web-app-title" content="e-Santé">
<meta name="theme-color" content="#00FA9A">
<meta name="msapplication-navbutton-color" content="#00FA9A">
<meta name="apple-mobile-web-app-status-bar-style" content="#00FA9A">
<meta name="msapplication-starturl" content="/">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- IOS Support -->
<link rel="icon" sizes="128x128" href="/images/icon-128x128.png">
<link rel="apple-touch-icon" sizes="128x128" href="/images/icon-128x128.png">
<link rel="icon" sizes="192x192" href="icon-192x192.png">
<link rel="apple-touch-icon" sizes="192x192" href="/images/icon-192x192.png">
<link rel="icon" sizes="256x256" href="/images/icon-256x256.png">
<link rel="apple-touch-icon" sizes="256x256" href="/images/icon-256x256.png">
<link rel="icon" sizes="384x384" href="/images/icon-384x384.png">
<link rel="apple-touch-icon" sizes="384x384" href="/images/icon-384x384.png">
<link rel="icon" sizes="512x512" href="/images/icon-512x512.png">
<link rel="apple-touch-icon" sizes="512x512" href="/images/icon-512x512.png">