<div class="container-fluid bg_color text-white">
    <div class="row">
        <div class="text-center mt-2">
            <span class="col-md-3 logo_space">
                <img src="{{ asset('images/eSante-05.jpg') }}" alt="logo" class="img_size">
            </span>

            <span class="col-md-3 logo_space">
                <img src="{{ asset('images/drapeau_bf.png') }}" alt="drapeau_bf" class="img_size">
            </span>

            <span class="col-md-3 logo_space">
                <img src="{{ asset('images/logo_cloudlyYours.jpg') }}" alt="logo_cloudlyYours" class="img_size">
            </span>

            <span class="col-md-3 logo_space">
                <img src="{{ asset('images/logo-africasys150.png') }}" alt="logo-africasys" class="img_size">
            </span>

            <span class="col-md-3">
                <img src="{{ asset('images/logo.png') }}" alt="logo_davycas" class="img_size">
            </span>
        </div>
    </div>

    <div class="text-center mt-2">
        © Copyright {{ Date('Y') }} - All Rights Reserved
    </div>
</div>
